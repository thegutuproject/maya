const {resolve} = require('path');

import {WDS_PORT} from './src/shared/config';
import {isProd} from './src/shared/util';

module.exports = {
  entry: ['./src/index.ts'],
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js']
  },
  output: {
    filename: 'js/bundle.js',
    path: resolve(__dirname, 'dist')
  },
  devServer: {
    port: WDS_PORT
  }
};
